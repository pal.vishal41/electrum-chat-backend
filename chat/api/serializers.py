from rest_framework import serializers

from chat.models import Chat
from chat.helpers import get_user_contact


class ContactSerializer(serializers.StringRelatedField):
    def to_internal_value(self, data):
        return data


class ChatSerializer(serializers.ModelSerializer):
    participants = ContactSerializer(many=True)

    class Meta:
        model = Chat
        fields = ('id', 'messages', 'participants')

    def create(self, validated_data):
        participants = validated_data.pop('participants')
        chat = Chat()
        chat.save()
        for username in participants:
            contact = get_user_contact(username)
            chat.participants.add(contact)
        chat.save()
        return chat
